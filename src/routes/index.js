import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";

import Home from "../pages/Home";
import Login from "../pages/Login";
import Register from "../pages/Register";

function ProtectedRoute({ element }) {
  const isAuthenticated = !!localStorage.getItem("token");

  return isAuthenticated ? element : <Navigate to="/login" />;
}

function UnprotectedRoute({ element }) {
  const isAuthenticated = !!localStorage.getItem("token");

  return isAuthenticated ? <Navigate to="/home" /> : element;
}

function RoutesConfig() {
  return (
    <Routes>
      {/* Rute UnprotectedRoute */}
      <Route path="login" element={<UnprotectedRoute element={<Home />} />} />
      <Route
        path="register"
        element={<UnprotectedRoute element={<Register />} />}
      />
      {/* Rute ProtectedRoute */}
      <Route path="home" element={<ProtectedRoute element={<Home />} />} />
      <Route path="*" element={<Navigate to="/login" />} />
    </Routes>
  );
}

export default RoutesConfig;
