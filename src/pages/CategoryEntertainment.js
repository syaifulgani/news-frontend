import React, { useState } from "react";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import "../index.css";
import CardNews1 from "../components/CardNews1";
import HighlightNews from "../components/HighlightNews";
import NewsList from "../components/NewsList";
import CardNews2 from "../components/CardNews2";

const CategoryPolitik = () => {
  const [colorEntertainment, setcolorEntertainment] = useState("orange");
  const [categoryEntertainment, setcategoryEntertainment] = useState(4);
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <Navbar colorEntertainment={colorEntertainment} />
      <CardNews1 categoryEntertainment={categoryEntertainment} />
      <div className="news-content">
        <HighlightNews categoryEntertainment={categoryEntertainment} />
        <NewsList categoryEntertainment={categoryEntertainment} />
      </div>
      <div className="news-title-recommendation">
        <h2>Rekomendasi Berita</h2>
        <CardNews2 categoryEntertainment={categoryEntertainment} />
      </div>
      <Footer />
    </div>
  );
};

export default CategoryPolitik;
