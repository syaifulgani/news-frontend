import React, { useState } from "react";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import "../index.css";
import CardNews1 from "../components/CardNews1";
import HighlightNews from "../components/HighlightNews";
import NewsList from "../components/NewsList";
import CardNews2 from "../components/CardNews2";

const MainPage = () => {
  const [categoryHome, setcategoryHome] = useState(0);
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <Navbar />
      <CardNews1 categoryHome={categoryHome} />
      <div className="news-content">
        <HighlightNews categoryHome={categoryHome} />
        <NewsList categoryHome={categoryHome} />
      </div>
      <div className="news-title-recommendation">
        <h2>Rekomendasi Berita</h2>
        <CardNews2 categoryHome={categoryHome} />
      </div>
      <Footer />
    </div>
  );
};

export default MainPage;
