import React, { useState } from "react";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import "../index.css";
import CardNews1 from "../components/CardNews1";
import HighlightNews from "../components/HighlightNews";
import NewsList from "../components/NewsList";
import CardNews2 from "../components/CardNews2";

const CategoryPolitik = () => {
  const [colorEkonomi, setcolorEkonomi] = useState("orange");
  const [categoryEkonomi, setcategoryEkonomi] = useState(3);
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <Navbar colorEkonomi={colorEkonomi} />
      <CardNews1 categoryEkonomi={categoryEkonomi} />
      <div className="news-content">
        <HighlightNews categoryEkonomi={categoryEkonomi} />
        <NewsList categoryEkonomi={categoryEkonomi} />
      </div>
      <div className="news-title-recommendation">
        <h2>Rekomendasi Berita</h2>
        <CardNews2 categoryEkonomi={categoryEkonomi} />
      </div>
      <Footer />
    </div>
  );
};

export default CategoryPolitik;
