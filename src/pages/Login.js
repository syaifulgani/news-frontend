import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { Button, Form, Input } from "antd";
import axios from "axios";
import "../App.css";
import useAuth from "../hooks/useAuth";
import Snackbar from "../components/Snackbar";

function Login() {
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const [error, setError] = useState("");
  const [snackType, setSnackType] = useState("");
  const { setToken } = useAuth();
  console.log(useAuth());

  const handleSubmit = (data) => {
    axios
      .post(`http://localhost:3001/auth/login`, data)
      .then((res) => {
        setToken(res.data.accessToken);
        navigate("/", { replace: true });
      })
      .catch((err) => {
        setError(err.response.data.error);
        setSnackType("error");
      });
  };

  return (
    <div className="container">
      <div className="form-box">
        <h2 style={{ textAlign: "center" }}>Login</h2>
        {error && (
          <div style={{ marginBottom: "2rem" }}>
            <Snackbar message={error} type={snackType} />
          </div>
        )}
        <Form form={form} onFinish={handleSubmit}>
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: "Please input your Username",
              },
            ]}
          >
            <Input prefix={<UserOutlined />} placeholder="Username" />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your Password!",
              },
            ]}
          >
            <Input.Password prefix={<LockOutlined />} placeholder="Password" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" block>
              Login
            </Button>
          </Form.Item>
        </Form>
        {error && <div className="error">{error}</div>}
        <p>
          Don't have an account? <Link to="/register">Register Here</Link>
        </p>
      </div>
    </div>
  );
}

export default Login;
