import React, { useState } from "react";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import "../index.css";
import CardNews1 from "../components/CardNews1";
import HighlightNews from "../components/HighlightNews";
import NewsList from "../components/NewsList";
import CardNews2 from "../components/CardNews2";

const CategoryPolitik = () => {
  const [colorPolitik, setcolorPolitik] = useState("orange");
  const [categoryPolitik, setcategoryPolitik] = useState(1);

  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <Navbar colorPolitik={colorPolitik} />
      <CardNews1 categoryPolitik={categoryPolitik} />
      <div className="news-content">
        <HighlightNews categoryPolitik={categoryPolitik} />
        <NewsList categoryPolitik={categoryPolitik} />
      </div>
      <div className="news-title-recommendation">
        <h2>Rekomendasi Berita</h2>
        <CardNews2 categoryPolitik={categoryPolitik} />
      </div>
      <Footer />
    </div>
  );
};

export default CategoryPolitik;
