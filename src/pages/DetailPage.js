import React, { Suspense } from "react";
import { Outlet } from "react-router";

const DetailPage = () => {
  return (
    <div>
      <h1>Ini detail page</h1>
      <main>
        <Suspense fallback={<div>Loading...</div>}>
          <Outlet />
        </Suspense>
      </main>
    </div>
  );
};

export default DetailPage;
